// ------------------------------------------------------------------- //
// --                                                               -- //
// --	Project:		Gothic 2 Online Utility Scripts             -- //
// --	Developers:		HammelGammel                                -- //
// --                                                               -- //
// ------------------------------------------------------------------- //

class Faction
{
    function isEnemyNameFaction(name)
    {
        local otherFac = getFaction(name);
        if (otherFac != null)
            return isEnemyFaction(otherFac);
        return false;
    }

    // ------------------------------------------------------------------- //

    function isEnemyFaction(fac)
    {
        return m_EnemyFactions.find(fac) != null;
    }

    // ------------------------------------------------------------------- //

    function addEnemyFaction(fac)
    {
        if (m_EnemyFactions.find(fac))
            return false;

        m_EnemyFactions.append(fac)
        return true;
    }

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //

    static function getFaction(name)
    {
        local lowerName = name.tolower();

        foreach (i, fac in m_Factions)
            if (fac.getName().tolower() == lowerName)
                return fac;

        return null;
    }

    // ------------------------------------------------------------------- //

    static function areFactionsEnemies(fac1, fac2)
    {
        if (fac1 == fac2)
            return false;

        return fac1.isEnemyFaction(fac2) || fac2.isEnemyFaction(fac1);
    }

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //
    // -- getters -- //
    function getName(){return m_Name;};
    function getEnemyFactions(){return m_EnemyFactions;};

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //

    constructor(name)
    {
        m_Name = name;

        m_EnemyFactions = [];
        m_Factions.append(this);
    }

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //

    m_Name = null;
    m_EnemyFactions = null;

    // ------------------------------------------------------------------- //
    // ------------------------------------------------------------------- //

    static m_Factions = [];
}
